var RedactorPlugins = RedactorPlugins || {};

RedactorPlugins.wordcount = {
    init: function(seperators) {
        // Add button to toolbar on redactor startup
        this.buttonAdd('wordcount', 'Word Count', function(name, title, obj, e) {
            // On click, create a modal showing the word count
            this.modalInit('Word Count', '<div id="redactor_modal_content"><p>This content contains '+ this.count(seperators) +' words.</p></div>', 300);
        });
        // Add separator before the button
        this.buttonAddSeparatorBefore('wordcount');
    },

    count: function(separators) {
        // Retrieve the editor's content
        var html = this.get(),
        // remove tabs and newlines and multiple spaces
            text = $(html).text().replace(/\t+/g, " ").replace(/\n/g, " ").replace(/\s+/g, " ");
        // Return the count - 1
        return text.split(' ').length - 1;
    }
};